package com.cascam.cdc.contract;

import com.cascam.cdc.OrderServiceConsumer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.autoconfigure.json.AutoConfigureJsonTesters;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.contract.stubrunner.spring.AutoConfigureStubRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.client.RestTemplate;

import static java.util.Objects.requireNonNull;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.cloud.contract.stubrunner.spring.StubRunnerProperties.StubsMode.LOCAL;
import static org.springframework.cloud.contract.stubrunner.spring.StubRunnerProperties.StubsMode.REMOTE;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
@AutoConfigureMockMvc
@AutoConfigureJsonTesters
/*@AutoConfigureStubRunner(
        stubsMode = LOCAL,
        ids = "com.cascam:service-provider:+:8090",
        repositoryRoot = "file:./stubs"
    )*/
@AutoConfigureStubRunner(
        stubsMode = LOCAL,
        ids = "com.cascam:service-provider:+:8090"
)

public class ContractConsumerTest {

    @Test
    void test() {
        String baseUrl = System.getenv("BASE_URL");
        //System.out.println("BASE URL IS "+ baseUrl);
    //    assertThat(baseUrl).isNotBlank(); // Ensure BASE_URL environment variable is set

        RestTemplate restTemplate = new RestTemplate();

        ResponseEntity<OrderServiceConsumer.Order> response = restTemplate.
                getForEntity("http://localhost:8090/assets",
                        OrderServiceConsumer.Order.class);
        /*ResponseEntity<OrderServiceConsumer.Order> response = restTemplate.
                getForEntity(baseUrl + "/assets",
                        OrderServiceConsumer.Order.class);*/
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        System.out.println(response.getBody());
        assertThat(requireNonNull(response.getBody()).getAssetNumber()).isEqualTo("1000002116153236");
        assertThat(requireNonNull(response.getBody()).getAvailableBalance()).isEqualTo(2000);
        assertThat(requireNonNull(response.getBody()).getExpirationDate()).isEqualTo("2040-12-31T23:59:59.999-06:00");
        assertThat(requireNonNull(response.getBody()).getId()).isEqualTo("dc582591-4b05-4ca4-9bb0-4d9dab56581d");
        assertThat(requireNonNull(response.getBody()).getStatus()).isEqualTo("OPEN");
        assertThat(requireNonNull(response.getBody()).getType()).isEqualTo("RTF");
        assertThat(requireNonNull(response.getBody()).getUom()).isEqualTo("USD");
    }
}
