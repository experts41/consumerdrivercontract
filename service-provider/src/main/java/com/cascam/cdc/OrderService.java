package com.cascam.cdc;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.stereotype.Service;

import java.util.Map;

import static org.springframework.http.ResponseEntity.ok;

@Service
public class OrderService {

    static Map<String, Order> orders = Map.of(
            "1", new Order("1000002116153236", 2000.00, "2040-12-31T23:59:59.999-06:00",
                    "dc582591-4b05-4ca4-9bb0-4d9dab56581d", "OPEN", "RTF", "USD")

    );



    public Order getOrder() {
       return orders.get("1");
    }

    @Data
    @AllArgsConstructor
    static class Order {
        private String assetNumber;

        private double availableBalance;

        private String expirationDate;

        private String id;

        private String status;

        private String type;

        private String uom;
    }
}
