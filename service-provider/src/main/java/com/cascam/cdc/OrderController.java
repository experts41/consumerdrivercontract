package com.cascam.cdc;

import com.cascam.cdc.OrderService.Order;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import static java.util.Objects.isNull;
import static org.springframework.http.ResponseEntity.notFound;
import static org.springframework.http.ResponseEntity.ok;

@RestController
public class OrderController {

    private final OrderService orderService;

    public OrderController(OrderService orderService) {
        this.orderService = orderService;
    }

    @GetMapping(value = "/assets", produces = "application/json")
    public ResponseEntity<Order> getOrder() {
        Order order = orderService.getOrder();
        if (isNull(order)) {
            return notFound().build();
        }
        return ok(order);
    }
}
