package contracts

import org.springframework.cloud.contract.spec.Contract

Contract.make {

    description "A request to create RTF"

    request {
        url "/assets"
        method GET()

    }

    response {
        status OK()
        headers {
            contentType applicationJson()
        }
        body(
                assetNumber: "1000002116153236",
                availableBalance: 2000,
                expirationDate: "2040-12-31T23:59:59.999-06:00",
                id: "dc582591-4b05-4ca4-9bb0-4d9dab56581d",
                status: "OPEN",
                type: "RTF",
                uom: "USD"
        )
    }
}

